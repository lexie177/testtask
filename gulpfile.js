const gulp = require('gulp');
const sass = require('gulp-sass');
const cssnano = require('gulp-cssnano');

gulp.task('css', () => {
  return gulp.src('static/styles.scss')
    .pipe(sass())
    .pipe(cssnano())
    .pipe(gulp.dest('src/', 'public/'));
});

gulp.task('watch', () => {
  gulp.watch('static/*.scss', gulp.series('css'));
});