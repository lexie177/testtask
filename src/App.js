import React from 'react';
import {Header, NavBar} from './Components';
import Demo from './Pages/Demo';

const App = () => {
  return (
    <div className="App">
      <Header/>
      <Demo/>
      <NavBar/>
    </div>
  );
}

export default App;
