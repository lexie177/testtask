import React, {useState} from 'react';

import {Gallery, GalleryControl, InfoBar, HelpBar} from '../../Components';

const Demo = () => {
  const [itemToShow, setItemToShow] = useState(0);

  return (
    <main className="main">
      <Gallery
        itemToShow={itemToShow}
        setItemToShow={setItemToShow}
      />
      <div className="content">
        <InfoBar/>
        <HelpBar/>
        <GalleryControl
          itemToShow={itemToShow}
          setItemToShow={setItemToShow}
        />
      </div>
    </main>
  )
}

export default Demo;