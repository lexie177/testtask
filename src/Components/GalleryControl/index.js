import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckSquare } from '@fortawesome/free-solid-svg-icons'

const IMAGES_GALLERY = [
  {
    color: '#ffcccc',
    title: 'Теплый',
    id: 0,
  },
  {
    color: '#ffffb3',
    title: 'Дневной',
    id: 1,
  },
  {
    color: '#ccccff',
    title: 'Холодный',
    id: 2,
  },
];

const GalleryControl = ({itemToShow, setItemToShow}) => {
  const inlineControls = () => IMAGES_GALLERY.map(({id, title, color}) =>
    <span
      style={{backgroundColor: color}}
      className={
        id === itemToShow ?
          'gallery__control gallery__control--active'
          :
          'gallery__control'
      }
      key={`${id}x${title}`}
      onClick={() => setItemToShow(id)}
    >
      <FontAwesomeIcon icon={faCheckSquare} className="gallery__control-mark"/>
    </span>
  );

  return (
    <div className="gallery__controls">
      {inlineControls()}
    </div>
  )
};

export default GalleryControl;