import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons'

const HelpBarModal = ({setModalOpened}) => {
  return (
    <div className="helpbar-modal">
      <button
        type="button"
        onClick={() => setModalOpened(false)}
        className="button helpbar-modal__button"
      >
        <FontAwesomeIcon icon={faChevronLeft}/>
        <span className="helpbar-modal__button-text">Вернуться</span>
      </button>
      <div className="helpbar-modal__content">
        <p className="helpbar-modal__text">Lorem Ipsum...</p>
      </div>
    </div>
  )
}

export default HelpBarModal;