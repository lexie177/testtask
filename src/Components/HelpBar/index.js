import React, {useState} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfo } from '@fortawesome/free-solid-svg-icons'
import HelpBarModal from './HelpBarModal';

const HelpBar = () => {
  const [ isModalOpened, setModalOpened ] = useState(false);
  return (
    <div className="helpbar">
      <button
        className="button helpbar__button"
        onClick={() => setModalOpened(true)}
      >
        <FontAwesomeIcon icon={faInfo}/>
      </button>
      <p className="helpbar__title">Выберите цвет свечения</p>
      {isModalOpened && <HelpBarModal setModalOpened={setModalOpened}/>}
    </div>
  )
}

export default HelpBar;