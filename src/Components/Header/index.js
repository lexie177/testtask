import React, {useState} from 'react';
import Menu from './HeaderMenu';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes, faBars, faShoppingCart } from '@fortawesome/free-solid-svg-icons'

const Header = () => {
  const [ isMenuOpen, setMenuOpen ] = useState(false);

  const toggleMenu = () => {
    setMenuOpen(!isMenuOpen);
  }
  return (
    <header className="header">
      <a href="/" className="header__logo">
        Logo placeholder
      </a>
      <div className="header__controls">
        <div className="cart header__cart">
          <span className="cart__amount">1</span>
          <FontAwesomeIcon icon={faShoppingCart} className="button header__cart-button"/>
        </div>
        <div className="header__menu">
          <button
            className="button header__menu-button"
            type="button"
            onClick={toggleMenu}
          >
            <FontAwesomeIcon icon={isMenuOpen ? faTimes : faBars} />
          </button>
          {isMenuOpen &&
            <Menu />
          }
        </div>
      </div>
    </header>
  )
}

export default Header;