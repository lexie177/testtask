import React from 'react';

const MENU_ITEMS = [
  {
    title: 'Обучающие видео',
    to: '/',
  },
  {
    title: 'Оформление заказа',
    to: '/',
  },
  {
    title: 'Оплата',
    to: '/',
  },
  {
    title: 'Доставка',
    to: '/',
  },
  {
    title: 'Гарантия',
    to: '/',
  },
  {
    title: 'Возврат',
    to: '/',
  },
  {
    title: 'Контакты',
    to: '/',
  },
  {
    title: 'Партнерам',
    to: '/',
  },
];

const HeaderMenu = () => 
  <ul className="header__nav">
    {MENU_ITEMS.map(({ title, to }) => 
      <li
        className="header__nav-item"
        key={`${title}-${to}`}
      >
        <a href={to} className="header__nav-link">{title}</a>
      </li>)}
  </ul>;

export default HeaderMenu;