import React, {useState} from 'react';

const NAV_ITEMS = [
  {
    title: 'Вариант кухни',
    id: 1,
  },
  {
    title: 'Размеры',
    id: 2,
  },
  {
    title: 'Сенсор',
    id: 3,
  },
  {
    title: 'Питающий кабель',
    id: 4,
  },
  {
    title: 'Блок питания',
    id: 5,
  },
  {
    title: 'Цвет свечения',
    id: 6,
  },
  {
    title: 'Монтаж',
    id: 7,
  },
  {
    title: 'Корзина',
    id: 8,
  },
];

const NavBar = () => {
const [ activeItemId, setActiveItemId ] = useState(6);

return (
  <div className="navbar">
    {NAV_ITEMS.map(({title, id}) =>
      <button
        className={
          activeItemId === id ?
          'button navbar__list-item navbar__list-item--active'
          :
          'button navbar__list-item'
        }
        key={`${title}-${id}`}
        onClick={() => setActiveItemId(id)}
      >
        {title}
      </button>
    )}
  </div>
  )
}

export default NavBar;