import React from 'react';

const PRODUCT_DATA = [
  {
    title: 'Класс',
    value: 'Standard',
    isValueLabel: true,
  },
  {
    title: 'Потребляемая мощность',
    value: '59 Вт',
    isValueLabel: false,
  },
  {
    title: 'Сила света',
    value: '3459 Люмен = 7,5 ламп накаливания по 40 Вт',
    isValueLabel: false,
  },
  {
    title: 'Гарантия',
    value: '2 года',
    isValueLabel: false,
  },
  {
    title: 'Монтаж',
    value: 'Да',
    isValueLabel: false,
  },
  {
    title: 'Итого сумма',
    value: '2594 рублей',
    isValueLabel: false,
  },
];

const InfoBar = () =>
  <div className="infobar">
    {PRODUCT_DATA.map(({title, value, isValueLabel}) =>
      <div className="infobar__row" key={`${title}-${value}`}>
        <span className="infobar__title">{`${title}:`}</span>
        <span
          className={
            isValueLabel ? 'infobar__value infobar__value--labelled' : 'infobar__value'
          }
        >
          <span className={isValueLabel ? 'labelled' : ''}>{value}</span>
        </span>
      </div>
    )}
  </div>;

export default InfoBar;