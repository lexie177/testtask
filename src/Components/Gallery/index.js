import React from 'react';

const IMAGES_GALLERY = [
  {
    color: '#ffcccc',
    title: 'Теплый',
    id: 0,
  },
  {
    color: '#ffffb3',
    title: 'Дневной',
    id: 1,
  },
  {
    color: '#ccccff',
    title: 'Холодный',
    id: 2,
  },
];

const Gallery = ({itemToShow, setItemToShow}) => {
  const inlineControls = () => IMAGES_GALLERY.map(({id, title}) =>
    <button
      type="button"
      className={
        id === itemToShow ?
          'button gallery__inline-control gallery__inline-control--active'
          :
          'button gallery__inline-control'
      }
      key={`${id}x${title}`}
      onClick={() => setItemToShow(id)}
    />
  );

  return (
    <div className="gallery">
      <div className="gallery__images">
        {IMAGES_GALLERY.map(({color, title, id}) =>
          <div
            style={{backgroundColor: color}}
            className={
              id === itemToShow ?
                'gallery__image gallery__image--active'
                :
                'gallery__image'
            }
            alt={title}
            key={`${title}-${id}`}
          />
        )}
      </div>
      <div className="gallery__inline-controls">
        {inlineControls()}
      </div>
    </div>
  );
};

export default Gallery;